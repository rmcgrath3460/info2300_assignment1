﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace RMAssignment2
{
    class RMValidation
    {
        // variable to track the first textbox with errors
        private static TextBox firstErrorTextBox = null;

        public static void FocusFirstError(TextBox errorTextBox)
        {
            if (firstErrorTextBox == null)
            {
                firstErrorTextBox = errorTextBox;
            }
        }

        public static void ClearFirstErrorTextBox()
        {
            firstErrorTextBox = null;
        }

        public static void SetFocusIfErrors()
        {
            if (firstErrorTextBox != null)
            {
                firstErrorTextBox.Focus();
            }                
        }

        public static string RMTitleGrammer(string title)
        {
            if (title == null)
            {
                return "";
            }
            else
            {
                TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
                return ti.ToTitleCase(title.Trim());
            }
        }

        public static bool RMISBNValidation(string ISBN)
        {
            if (String.IsNullOrEmpty(ISBN))
            {
                return false;
            }
            else
            {
                if (ISBN.Length == 13)
                {
                    return ISBN.All(char.IsDigit);
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool RMPhoneNumberValidation(string phoneNumber)
        {
            if (String.IsNullOrEmpty(phoneNumber))
            {
                return false;
            }
            else
            {
                // matches 123-456-7890, 123.456.7890, 1234567890, but not 123.456-7890
                Regex phonePattern = new Regex(@"^\d{3}([-.]?)\d{3}\1\d{4}$");
                return phonePattern.IsMatch(phoneNumber);
            }
        }

        public static bool RMPostalCodeValidation(ref string postalCode)
        {
            if (String.IsNullOrEmpty(postalCode))
            {
                return false;
            }
            else
            {
                Regex postalCodePattern = new Regex(@"^[a-zA-Z]\d[a-zA-Z]\s?\d[a-zA-Z]\d$");
                
                if(postalCodePattern.IsMatch(postalCode))
                {
                    if(postalCode[3] == ' ')
                    {
                        postalCode = postalCode.ToUpper();
                    }
                    else
                    {
                        postalCode = postalCode.Insert(3, " ").ToUpper();
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
