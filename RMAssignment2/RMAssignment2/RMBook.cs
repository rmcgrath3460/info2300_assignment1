﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Net.Mail;

namespace RMAssignment2
{
    public partial class RMBook : Form
    {
        public RMBook()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnPreFill_Click(object sender, EventArgs e)
        {
            txtBookTitle.Text = "War and Peace";
            txtISBN.Text = "9781788886529";
            txtAuthorName.Text = "Leo Tolstoy";
            txtDatePublished.Text = "19-Mar-2019";
            txtMailingAddress.Text = "123 New";
            txtStreet.Text = "Street";
            txtTown.Text = "Burlington";
            txtCountry.Text = "Canada";
            txtProvince.Text = "ON";
            txtPostalCode.Text = "A1A 1A1";
            txtHomePhone.Text = "123-456-7890";
            txtCellPhone.Text = "321.654.0987";
            txtEmail.Text = "rmcgrath3460@conestogac.on.ca";
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            lblErrors.Text = "";
            RMValidation.ClearFirstErrorTextBox();

            // validate textboxes
            ValidateTitle();
            ValidateISBN();
            ValidateAuthorName();
            ValidateDatePublished();
            ValidateMailingAddress();
            ValidateStreet();
            ValidateTown();
            ValidateCountry();
            ValidateProvince();
            ValidatePostalCode();
            ValidateHomePhone();
            ValidateCellPhone();
            ValidateEmail();

            // set focus to the first textbox with errors
            RMValidation.SetFocusIfErrors();
        }

        // book title is required, cannot be blank, "Submit" sets text equal to output from RMTitleGrammer
        public void ValidateTitle()
        {
            string formattedTitle = RMValidation.RMTitleGrammer(txtBookTitle.Text);
            txtBookTitle.Text = formattedTitle;

            if (formattedTitle == "")
            {
                lblErrors.Text += "Book Title is required\n";
                RMValidation.FocusFirstError(txtBookTitle);
            }
        }

        // ISBN is required, validated by RMISBNValidation, cannot be blank, must be 13 digits
        public void ValidateISBN()
        {
            if (!RMValidation.RMISBNValidation(txtISBN.Text))
            {
                lblErrors.Text += "ISBN Code is required and must be exactly 13 digits\n";
                RMValidation.FocusFirstError(txtISBN);
            }
        }

        // author name is required, must contain more than one name (delimited by space or comma)
        public void ValidateAuthorName()
        {
            string formattedAuthorName = RMValidation.RMTitleGrammer(txtAuthorName.Text);
            txtAuthorName.Text = formattedAuthorName;

            char[] delimiters = { ' ', ',' };
            string[] names = formattedAuthorName.Split(delimiters);
            int numberOfNames = names.Length;

            if ((formattedAuthorName == "") || (numberOfNames < 2))
            {
                lblErrors.Text += "Author Full Name is required and must contain at least two names\n";
                RMValidation.FocusFirstError(txtAuthorName);
            }
        }

        // date published is optional, if provided must be in the format dd-mmm-yyyy, cannot be in the future
        public void ValidateDatePublished()
        {
            if (txtDatePublished.Text != "")
            {
                DateTime dateValue;
                bool dateFormatValid = DateTime.TryParseExact(txtDatePublished.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue);
                
                if (!dateFormatValid || (dateValue > DateTime.Now))
                {
                    lblErrors.Text += "Date Published is invalid (e.g., must be in the format dd-MMM-yyyy and cannot be in the future)";
                    RMValidation.FocusFirstError(txtDatePublished);
                }
            }
        }

        // mailing address required if email address is not provided, "Submit" sets text equal to output from RMTitleGrammer
        public void ValidateMailingAddress()
        {
            string formattedMailingAddress = RMValidation.RMTitleGrammer(txtMailingAddress.Text);
            txtMailingAddress.Text = formattedMailingAddress;

            if (txtEmail.Text == "")
            {
                if (formattedMailingAddress == "")
                {
                    lblErrors.Text += "Mailing Address is required if Email is not provided\n";
                    RMValidation.FocusFirstError(txtMailingAddress);
                }
            }
        }

        // street is required if email address is not provided, "Submit" sets text equal to output from RMTitleGrammer
        public void ValidateStreet()
        {
            string formattedStreet = RMValidation.RMTitleGrammer(txtStreet.Text);
            txtStreet.Text = formattedStreet;

            if (txtEmail.Text == "")
            {
                if (formattedStreet == "")
                {
                    lblErrors.Text += "Street is required if Email is not provided\n";
                    RMValidation.FocusFirstError(txtStreet);
                }
            }
        }

        // town is required if email address is not provided, "Submit" sets text equal to output from RMTitleGrammer
        public void ValidateTown()
        {
            string formattedTown = RMValidation.RMTitleGrammer(txtTown.Text);
            txtTown.Text = formattedTown;

            if (txtEmail.Text == "")
            { 
                if (formattedTown == "")
                {
                    lblErrors.Text += "Town is required if Email is not provided\n";
                    RMValidation.FocusFirstError(txtTown);
                }
            }
        }

        // country is required if email address is not provided, "Submit" sets text equal to output from RMTitleGrammer
        public void ValidateCountry()
        {
            string formattedCountry = RMValidation.RMTitleGrammer(txtCountry.Text);
            txtCountry.Text = formattedCountry;

            if (txtEmail.Text == "")
            {
                if (formattedCountry == "")
                {
                    lblErrors.Text += "Country is required if Email is not provided\n";
                    RMValidation.FocusFirstError(txtCountry);
                }
            }
        }

        // province is required if email address is not provided, two letters only, shifted to upper case
        public void ValidateProvince()
        {
            string formattedProvince = (txtProvince.Text).ToUpper();
            txtProvince.Text = formattedProvince;

            if (txtEmail.Text == "")
            {
                if (formattedProvince == "")
                {
                    lblErrors.Text += "Province is required if Email is not provided\n";
                    RMValidation.FocusFirstError(txtProvince);
                }
                else if (!formattedProvince.All(char.IsLetter) || formattedProvince.Length != 2)
                {
                    lblErrors.Text += "Province must be two letters only\n";
                    RMValidation.FocusFirstError(txtProvince);
                }
            }
        }

        // postal code is required, validated with RMPostalCodeValidation
        public void ValidatePostalCode()
        {
            string formattedPostalCode = txtPostalCode.Text;

            if (formattedPostalCode == "")
            {
                lblErrors.Text += "Postal Code is required\n";
                RMValidation.FocusFirstError(txtPostalCode);
            }
            else if (!RMValidation.RMPostalCodeValidation(ref formattedPostalCode))
            {
                lblErrors.Text += "Postal Code must be in the format A1A 1A1\n";
                RMValidation.FocusFirstError(txtPostalCode);
            }
            
            txtPostalCode.Text = formattedPostalCode;
        }

        // either home phone, cell phone, or both must be provided, validated with RMPhoneNumberValidation
        public void ValidateHomePhone()
        {
            if (txtHomePhone.Text == "" && txtCellPhone.Text == "")
            {
                lblErrors.Text += "At least one phone number must be provided";
                RMValidation.FocusFirstError(txtHomePhone);
            }
            else if (!RMValidation.RMPhoneNumberValidation(txtHomePhone.Text))
            {
                lblErrors.Text += "Home Phone must be in a valid format (e.g. 123-456-7890)";
                RMValidation.FocusFirstError(txtHomePhone);
            }
        }

        public void ValidateCellPhone()
        {
            if (!RMValidation.RMPhoneNumberValidation(txtCellPhone.Text) && txtCellPhone.Text != "")
            {
                lblErrors.Text += "Cell Phone must be in a valid format (e.g. 123-456-7890)";
                RMValidation.FocusFirstError(txtCellPhone);
            }
        }

        // email is optional, validate by trying to instantiate a new MailAddress from System.Net.Mail using the control's Text
        public void ValidateEmail()
        {
            string formattedEmail = txtEmail.Text.ToLower();
            txtEmail.Text = formattedEmail;

            if(formattedEmail != "")
            {
                try
                {
                    MailAddress testEmail = new MailAddress(formattedEmail);
                }
                catch (Exception ex)
                {
                    lblErrors.Text += "Email provided is invalid";
                    RMValidation.FocusFirstError(txtEmail);
                }
            }            
        }
    }
}
