# RMAssignment2 

RMAssignment2 is a simple .NET Framework application which was used to practice validation of text fields.

## Installation

Open the RMAssignment2.sln file in Visual Studio and hit Start to run the program.

## Usage

To test the validations, enter data into the text fields and hit the Submit button. Red text will appear in the blank area at the bottom of the window to alert the user of any failed validations. 

To speed up testing, the Pre-Fill button can be used to fill all fields with valid data.

## License
[MIT](https://choosealicense.com/licenses/mit/)

I chose to use an MIT license for this project because it allows others to use this code with next to no restrictions (even if used for commercial purposes), as long as I am credited for my code.